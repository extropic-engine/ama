Gem::Specification.new do |s|
  s.name        = 'ama'
  s.version     = '0.0.0'
  s.date        = '2014-12-16'
  s.summary     = 'Ama'
  s.description = 'Distributed AI'
  s.authors     = ['Josh Stewart']
  s.email       = 'extropic.engine@gmail.com'
  s.files       = ['lib/ama.rb', 'lib/brain/memory-fragment.rb']
  s.homepage    = 'http://www.extropicstudios.com'
  s.license     = 'GPLv3'
  s.executables << 'ama'
  s.add_runtime_dependency 'json'
end
