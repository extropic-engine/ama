require 'json'

class Ama
  def self.main
    return self.read_manifest if File::exist? '.ama'

    Dir.entries '.' do |f|
      if f =~ /^.*\.zip$/i
        puts 'found something cool... ' + f
      else
        puts 'found something boring: ' + f
      end
    end
  end

  def self.read_manifest
    manifest = JSON.parse(open('.ama').read)
    if manifest.package
      puts 'there is a package here named' + manifest.package.name
      puts 'i could back it up for you...'
      response = gets.chomp
      if response == 'ok'
        puts "uploading #{manifest.package.name} to the net"
      end
    end
    puts 'i found something mysterious:'
    puts manifest
  end
end

require 'brain/memory-fragment'
