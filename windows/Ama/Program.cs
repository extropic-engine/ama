﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Extropic {
    namespace Ama
    {
        class CommandLine
        {
            static void Ma(string[] args)
            {
                if (File.Exists(".ama"))
                {
                    FragmentManifest fm = JsonConvert.DeserializeObject<FragmentManifest>((new StreamReader(".ama")).ReadToEnd());
                    Console.WriteLine("name: " + fm.name);
                    Console.WriteLine("type: " + fm.type);
                }
                else
                {
                    Console.WriteLine("nothing here");
                }
            }
        }
    }
}
