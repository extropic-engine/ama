# ama

computers for feelers

## brainstorming

- voice controlled raspberry pi
- wearable computing: https://www.adafruit.com/category/65

# AI notes

Virtual assistant in the true sense of the word. Evolution of Freya. Open source / open ecosystem alternative to Siri/Google Now/Cortana

## Characteristics:

- Omnipresent throughout the operating system and computing environment
- Presence in virtual worlds (offline and online)

## Functionality

### Package Manager

### Cloud Synchronizer

## Implementation

### Voice Recognition

- Windows: SpeechRecognitionEngine in .NET
- OS X: NSSpeechRecognizer
- http://nshipster.com/nslinguistictagger/

### Speech Synthesis

- Windows: SpeechSynthesizer in .NET
- OS X: 'say' command or NSSpeechSynthesizer

## TODO

- [ ] Read and write zip files
- [ ] Push and pull w/ google drive
